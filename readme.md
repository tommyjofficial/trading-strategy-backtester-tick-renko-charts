## Trading Strategy Backtester

#### Backtester for testing trading strategies.

#### Combines tick and renko charts into one. New bar when either renko limit is reach or a defined number of ticks is reach. 

#### Historical data either from CSV files or direcly from MetaTrader platform

#### Uses Python Wrapper for MQL5